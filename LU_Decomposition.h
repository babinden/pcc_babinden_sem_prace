//
// Created by Denys Babin on 04.01.2024.
//

#ifndef PCC_BABINDEN_SEM_PRACE_LU_DECOMPOSITION_H
#define PCC_BABINDEN_SEM_PRACE_LU_DECOMPOSITION_H

#include <vector>

class LU_Decomposition {
private:
    std::vector<std::vector<double>> L;
    std::vector<std::vector<double>> U;
    std::vector<std::vector<double>> original_matrix;
    bool is_decomposed;

public:
    LU_Decomposition(const std::vector<std::vector<double>>& matrix);
    void decompose();
    std::vector<std::vector<double>> getL() const;
    std::vector<std::vector<double>> getU() const;
    bool isDecomposed() const;
    std::vector<double> forwardSubstitution(const std::vector<double>& b) const;
    std::vector<double> backwardSubstitution(const std::vector<double>& y) const;
    std::vector<std::vector<double>> inverse() const;

};

#endif //PCC_BABINDEN_SEM_PRACE_LU_DECOMPOSITION_H
