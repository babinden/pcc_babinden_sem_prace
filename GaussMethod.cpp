//
// Created by Denys Babin on 04.01.2024.
//

#include "GaussMethod.h"
#include <iostream>

std::vector<std::vector<double>> gaussMethod(std::vector<std::vector<double>> matrix) {
    int n = matrix.size();
    std::vector<std::vector<double>> inverse(n, std::vector<double>(n, 0.0));

    // Formation of the identity matrix
    for (int i = 0; i < n; i++) {
        inverse[i][i] = 1.0;
    }

    // Straight stroke
    for (int i = 0; i < n; i++) {
        // Normalization of string i
        double diagElem = matrix[i][i];
        for (int j = 0; j < n; j++) {
            matrix[i][j] /= diagElem;
            inverse[i][j] /= diagElem;
        }

        // Zeroing column i in other rows
        for (int j = 0; j < n; j++) {
            if (j != i) {
                double factor = matrix[j][i];
                for (int k = 0; k < n; k++) {
                    matrix[j][k] -= factor * matrix[i][k];
                    inverse[j][k] -= factor * inverse[i][k];
                }
            }
        }
    }

    return inverse;
}
