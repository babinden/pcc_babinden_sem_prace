//
// Created by Denys Babin on 04.01.2024.
//
#include "LU_Decomposition.h"
#include <stdexcept>

// Constructor for LU Decomposition class
LU_Decomposition::LU_Decomposition(const std::vector<std::vector<double>>& matrix)
        : original_matrix(matrix), is_decomposed(false) {

    // Check if the matrix is square
    if (matrix.empty() || matrix.size() != matrix[0].size()) {
        throw std::invalid_argument("Matrix must be square.");
    }

    // Initialize L and U matrices with zeros
    L = std::vector<std::vector<double>>(matrix.size(), std::vector<double>(matrix.size(), 0.0));
    U = std::vector<std::vector<double>>(matrix.size(), std::vector<double>(matrix.size(), 0.0));
}

// Function to decompose the matrix into L and U
void LU_Decomposition::decompose() {
    // Check if the matrix is already decomposed
    if (is_decomposed) {
        return;
    }

    size_t n = original_matrix.size();

    // Decomposing matrix into L and U
    L = std::vector<std::vector<double>>(n, std::vector<double>(n, 0.0));
    U = std::vector<std::vector<double>>(n, std::vector<double>(n, 0.0));

    for (size_t i = 0; i < n; i++) {
        L[i][i] = 1; // diagonals in L == 1
        for (size_t j = i; j < n; j++) {
            double sum = 0.0;
            for (size_t k = 0; k < i; k++) {
                sum += L[i][k] * U[k][j];
            }
            U[i][j] = original_matrix[i][j] - sum;
        }

        for (size_t j = i + 1; j < n; j++) {
            double sum = 0.0;
            for (size_t k = 0; k < i; k++) {
                sum += L[j][k] * U[k][i];
            }
            L[j][i] = (original_matrix[j][i] - sum) / U[i][i];
        }
    }
    is_decomposed = true;
}

// Getter for L matrix
std::vector<std::vector<double>> LU_Decomposition::getL() const {
    if (!is_decomposed) {
        throw std::logic_error("LU Decomposition has not been performed.");
    }
    return L;
}

// Getter for U matrix
std::vector<std::vector<double>> LU_Decomposition::getU() const {
    if (!is_decomposed) {
        throw std::logic_error("LU Decomposition has not been performed.");
    }
    return U;
}

// Check if the matrix is decomposed
bool LU_Decomposition::isDecomposed() const {
    return is_decomposed;
}

// Forward substitution to solve Ly=b
std::vector<double> LU_Decomposition::forwardSubstitution(const std::vector<double>& b) const {
    size_t n = L.size();
    std::vector<double> z(n, 0.0);

    for (size_t i = 0; i < n; ++i) {
        double sum = 0.0;
        for (size_t j = 0; j < i; ++j) {
            sum += L[i][j] * z[j];
        }
        z[i] = b[i] - sum;
    }

    return z;
}

// Backward substitution to solve Ux=y
std::vector<double> LU_Decomposition::backwardSubstitution(const std::vector<double>& y) const {
    size_t n = U.size();
    std::vector<double> x(n, 0.0);

    for (int i = n - 1; i >= 0; --i) {
        double sum = 0.0;
        for (size_t j = i + 1; j < n; ++j) {
            sum += U[i][j] * x[j];
        }
        x[i] = (y[i] - sum) / U[i][i];
    }

    return x;
}

// Function to calculate the inverse of the original matrix
std::vector<std::vector<double>> LU_Decomposition::inverse() const {
    if (!is_decomposed) {
        throw std::logic_error("LU Decomposition has not been performed.");
    }

    size_t n = L.size();
    std::vector<std::vector<double>> inv(n, std::vector<double>(n, 0.0));

    // Calculating the inverse matrix
    for (size_t i = 0; i < n; i++) {
        std::vector<double> e(n, 0.0);
        e[i] = 1.0; // Unit vector
        std::vector<double> y = forwardSubstitution(e);
        std::vector<double> x = backwardSubstitution(y);
        for (size_t j = 0; j < n; j++) {
            inv[j][i] = x[j];
        }
    }
    return inv;
}
