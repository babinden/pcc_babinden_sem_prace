# PCC_babinden_sem_prace

## Zadání


Úkolem mého projektu bylo implementovat dva algoritmy pro výpočet inverzní matice.
Použité algoritmy: Gaussova metoda a LU rozklad. 
Poté porovnat tyto dva algoritmy.

## Vzorová Vstupní data

V projektu je 10 souborů, které budou zapsány do tabulky s výsledky.
Soubory budou pojmenovány
matrix_1.txt, matrix_2.txt, matrix_3.txt a tak dále.
matrix_1.txt, matrix_2.txt - v těchto souborech budou malé matice, které lze ověřit na internetu pro správnost a ujistit se, že algoritmy poskytují správné výsledky.

matrix.txt - to je hlavní soubor, ze kterého bude čtena matice, aby se našla inverzní matice.
Bude potřeba zkopírovat text z příkladu, například z matrix_1.txt, a vložit jej do matrix.txt.

Dále je zde třída GenerateMatrix, která umožňuje generovat náhodnou čtvercovou matici s určenou velikostí, v rozsahu čísel od -100 do 100, do souboru matrix.txt. (Všechny příkazy budou dále.)

## Popis implementace


V mém projektu je jednoduchá architektura.
Všechny potřebné soubory se nacházejí ve složce PCC_babinden_sem_prace.
Tam se také nachází složka build, ze které budeme spouštět náš projekt po jeho sestavení.
1. CMakeLists.txt - soubor pro sestavení projektu. 
2. GaussMethod.cpp - implementuje Gaussovu metodu. 
3. LU_Decomposition.cpp - implementuje LU rozklad.
4. main.cpp - hlavní soubor pro spuštění programu. 
5. matrix.txt - soubor, ze kterého bude čtena matice pro použití určitého algoritmu pro nalezení inverzní matice. 
6. matrix_XX.txt - ukázkové soubory. Pro kontrolu bude třeba zkopírovat text z těchto souborů do souboru matrix.txt.


## Popis funkčnost

1. Program zahrnuje dva algoritmy pro výpočet inverzních matic:

- Gaussova metoda: Klasický algoritmus pro nalezení inverzní matice pomocí elementárních řádkových operací. 
- LU rozklad: Výpočet inverzní matice rozkladem původní matice na dvě trojúhelníkové matice (L a U).
   
2. Čtení matic ze souboru: Program je schopen číst matice z externích souborů. To umožňuje snadno zpracovávat různé sady dat bez nutnosti změny zdrojového kódu.

3. Generování matic: Pomocí nástroje GenerateMatrix mohou uživatelé generovat náhodné čtvercové matice dané velikosti pro testování algoritmů. Rozsah hodnot prvků matice je od -100 do 100.

4. Flexibilita ve výběru algoritmu: Uživatelé si mohou při spuštění programu vybrat mezi Gaussovou metodou a LU rozkladem, což poskytuje flexibilitu a umožňuje srovnávat efektivitu obou metod.

5. Validace vstupních dat: Program kontroluje vstupní data pro správnost, včetně toho, zda poskytnuté matice jsou čtvercové, což je nezbytnou podmínkou pro nalezení inverzní matice.

6. Výstup výsledků: Po zpracování vstupních dat a provedení výpočtů program vypíše inverzní matici, což umožňuje uživatelům analyzovat a využívat získané výsledky.

## Ovládání aplikace

"Použiti programu:"
- "--help - Help"
- "method Gauss - spuštění programu pomocí Gaussovy metody"
- "method LU - spuštění programu pomocí rozkladu LU"
- "GenerateMatrix XX - generace nové matice v soubor matrix.txt. XX - int čislo."
}

## Přístroj pro mněření
Macbook Pro M1
16 jádrovém
16 CPU

## Vysledky
![Vysledky](images/chart.png)

Z grafu je patrné, že na samém začátku LU rozklad a Gaussova metoda vyžadují přibližně stejný čas. Pokud je matice větší než 500, pak je již vidět rozdíl. V tomto případě je rozklad LU přibližně o 10% rychlejší. U matice větší než 2000 je již patrný významný rozdíl a předpokládá se, že tento rozdíl bude rychle narůstat.

Valid Valgrind

![Vysledky](images/valgrind1.png)
![Vysledky](images/valgrind2.png)
![Vysledky](images/valgrind3.png)


