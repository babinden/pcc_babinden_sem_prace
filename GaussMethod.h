//
// Created by Denys Babin on 04.01.2024.
//

#ifndef PCC_BABINDEN_SEM_PRACE_GAUSSMETHOD_H
#define PCC_BABINDEN_SEM_PRACE_GAUSSMETHOD_H

#include <vector>

std::vector<std::vector<double>> gaussMethod(std::vector<std::vector<double>> matrix);

#endif //PCC_BABINDEN_SEM_PRACE_GAUSSMETHOD_H
