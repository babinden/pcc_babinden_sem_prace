#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "LU_Decomposition.h"
#include "GaussMethod.h"
#include <chrono>

std::vector<std::vector<double>> readMatrixFromFile(const std::string &filename) {
    std::vector<std::vector<double>> matrix;
    std::ifstream file(filename);

    if (!file.is_open()) {
        std::cerr << "Can not open this file." << std::endl;
        return matrix;
    }

    std::string line;
    size_t expectedColumns = 0;
    size_t rowNumber = 0;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<double> row;
        double value;

        while (iss >> value) {
            row.push_back(value);
        }

        if (rowNumber == 0) {
            // columns in row
            expectedColumns = row.size();
        }

        if (row.size() != expectedColumns) {
            std::cerr << "The matrix is not square." << std::endl;
            return std::vector<std::vector<double>>();
        }

        if (!row.empty()) {
            matrix.push_back(row);
            rowNumber++;
        }
    }

    if (matrix.size() != expectedColumns) {
        std::cerr << "The matrix is not square." << std::endl;
        return std::vector<std::vector<double>>();
    }

    file.close();
    return matrix;
}

void printMatrix(const std::vector<std::vector<double>>& matrix) {
    for (const auto& row : matrix) {
        for (double elem : row) {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
}

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void printHelp() {
    std::cout << "Použiti programu:" << std::endl;
    std::cout << "--help - Help" << std::endl;
    std::cout << "přikazy:" << std::endl;
    std::cout << "method Gauss - spuštění programu pomocí Gaussovy metody" << std::endl;
    std::cout << "method LU - spuštění programu pomocí rozkladu LU" << std::endl;
    std::cout << "GenerateMatrix XX - generace nové matice v soubor matrix.txt. _XX_ - int čislo." << std::endl;
}

void generateMatrix(int size) {
    std::ofstream file("../matrix.txt");
    if (!file.is_open()) {
        std::cerr << "Can not open the file." << std::endl;
        return;
    }

    std::srand(std::time(nullptr));

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            int random_number = std::rand() % 201 - 100;
            file << random_number << " ";
        }
        file << std::endl;
    }

    file.close();
    std::cout << "Matrix of size " << size << "x" << size << " was generated in 'matrix.txt'." << std::endl;
}

int main() {
    std::string command;
    std::cout << "Zadejte příkaz (nebo --help pro nápovědu): ";

    while (std::getline(std::cin, command)) {
        std::string filename = "../matrix.txt";
        std::vector<std::vector<double>> matrix = readMatrixFromFile(filename);

        if (command == "--help") {
            printHelp();
        } else if (command == "method LU" || command == "method Gauss") {
            if (matrix.empty()) {
                std::cerr << "Matrix is empty or file read error occurred." << std::endl;
                continue; // wait another command
            }

            std::vector<std::vector<double>> inverseMatrix;
            auto start = std::chrono::high_resolution_clock::now(); // start timer

            try {
                if (command == "method LU") {
                    LU_Decomposition lu(matrix);
                    lu.decompose();
                    inverseMatrix = lu.inverse();
                } else if (command == "method Gauss") {
                    inverseMatrix = gaussMethod(matrix);
                }

                auto finish = std::chrono::high_resolution_clock::now(); // end timer
                auto time = to_ms(finish - start); // time in milliseconds

                std::cout << "Inverse Matrix:" << std::endl;
                printMatrix(inverseMatrix);
                std::cout << "Needed " << time.count() << " ms to finish.\n"; // print time
            } catch (const std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
                continue; // wait another command
            }

        } else if (command.rfind("GenerateMatrix", 0) == 0) {
            std::stringstream ss(command);
            std::string cmd;
            int matrixSize;
            ss >> cmd >> matrixSize;
            if (matrixSize > 0) {
                generateMatrix(matrixSize);
            } else {
                std::cerr << "Invalid matrix size." << std::endl;
            }
        } else {
            std::cout << "Neznámý příkaz. Zadejte --help pro nápovědu." << std::endl;
        }

        std::cout << "\nZadejte následující příkaz (nebo --help pro nápovědu): ";
    }

    return 0;
}